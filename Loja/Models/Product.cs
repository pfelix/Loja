﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Loja.Models
{
    public class Product
    {
        [Key]   // Chama-se DataAnnotations
                // Isto é para indicar chave primária
                // Necessita using System.ComponentModel.DataAnnotations;

                // public int ID { get; set; } - No inicio do projeto era isto depois alteramos para ProductId
                // Esta situação foi feita para mostar a migração de Base de dados, para não termos que apagar a Base de dados sempre que fazemos alguma alteração nos modelos
        public int ProductId { get; set; }

        public string Description { get; set; }

        public decimal Price { get; set; }
        
        public DateTime LastBuy { get; set; }

        public float Stock { get; set; }

        public string Remarks { get; set; }

    }
}