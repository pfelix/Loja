﻿using Loja.Context;
using Loja.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace Loja.Controllers
{
    public class ProductController : Controller
    {
        // Igual ao processo do DataContext, mas neste caso somos nós que criamos o DataContext
        private StoreContext db = new StoreContext();

        // GET: Product
        public ActionResult Index()
        {
            return View(db.Products.ToList());
        }

        // GET: Product/Details/5
        public ActionResult Details(int? id) // ? - Para aceitar valores null
        {
            // Validação para quando é intoduzido manualmente na barra de endereços e falta o ID
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // Procurar o produto pelo ID
            var product = db.Products.Find(id);

            // Validação para quando o id não existe
            if (product == null)
            {
                return HttpNotFound();
            }

            return View(product);
        }

        // GET: Product/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Product/Create
        [HttpPost]
        public ActionResult Create(Product product)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    db.Products.Add(product);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }

                return View(product);
            }
            catch
            {
                return View(product);
            }
        }

        // GET: Product/Edit/5
        public ActionResult Edit(int? id)
        {
            // Validação para quando é intoduzido manualmente na barra de endereços e falta o ID
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // Procurar o produto pelo ID
            var product = db.Products.Find(id);

            // Validação para quando o id não existe
            if (product == null)
            {
                return HttpNotFound();
            }

            return View(product);
        }

        // POST: Product/Edit/5
        [HttpPost]
        public ActionResult Edit(Product product)
        {
            try
            {
                // Valiada se os dados que está a receber são um Product
                if(ModelState.IsValid)
                {
                    // Guarda as alterações
                    db.Entry(product).State = EntityState.Modified;

                    // Guarda na Base de dados
                    db.SaveChanges();

                    return RedirectToAction("Index");
                }

                return View(product);
            }
            catch
            {
                return View(product);
            }
        }

        // GET: Product/Delete/5
        public ActionResult Delete(int? id)
        {
            // Validação para quando é intoduzido manualmente na barra de endereços e falta o ID
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            // Procurar o produto pelo ID
            var product = db.Products.Find(id);

            // Validação para quando o id não existe
            if (product == null)
            {
                return HttpNotFound();
            }

            return View(product);
        }

        // POST: Product/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, Product product)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    product = db.Products.Find(id);

                    // Valida se id existe nos produtos
                    if (product == null)
                    {
                        return HttpNotFound();
                    }

                    // Remove produto
                    db.Products.Remove(product);

                    // Salva na base de dados
                    db.SaveChanges();

                    return RedirectToAction("Index");
                }

                return View(product);
            }
            catch
            {
                return View(product);
            }
        }
    }
}
