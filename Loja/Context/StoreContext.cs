﻿using Loja.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Loja.Context
{
    public class StoreContext: DbContext // Herda da Entity é necessário using System.Data.Entity;
    {
        // Entidade que representa uma tabela
        // É responsável por criar a tabla Prodcts, com as Propriedades que estão na Classe Product
        public DbSet<Product> Products { get; set; }
            

    }
}